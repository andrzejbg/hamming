# Hamming #


Implementation of CUDA assignment: given a set of long bit sequences, find all pairs with Hamming distance equal to 1. 
Only pairs with difference of their sums of bits equal to 1 are compared, others definitely have different distance.

Works with CUDA 8.0.