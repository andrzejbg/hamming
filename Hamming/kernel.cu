#include "cuda_runtime.h"
#include "device_launch_parameters.h"
#include <iostream>
#include <math.h>
#include <time.h>
#include <stdio.h>
#include <bitset>
#include <stdint.h>
#include <inttypes.h>

#define BLOCK_SIZE 512

const uint64_t m1 = 0x55; //binary: 0101...
const uint64_t m2 = 0x33; //binary: 00110011..
const uint64_t m4 = 0x0f; //binary:  4 zeros,  4 ones ...

__host__ __device__ int bitCount(uint64_t x)
{
	x -= (x >> 1) & m1;             //put count of each 2 bits into those 2 bits
	x = (x & m2) + ((x >> 2) & m2); //put count of each 4 bits into those 4 bits 
	x = (x + (x >> 4)) & m4;        //put count of each 8 bits into those 8 bits 
	x += x >> 8;  //put count of each 16 bits into their lowest 8 bits
	x += x >> 16;  //put count of each 32 bits into their lowest 8 bits
	x += x >> 32;  //put count of each 64 bits into their lowest 8 bits
	return x & 0x7f;
}

__global__ void comparePair(int n, const uint64_t *x, const uint64_t *y, int* distance, const int i)
{
	int index = blockIdx.x * blockDim.x + threadIdx.x;
	if (index < n && x[index] != y[index] && distance[i] < 2)
	{
		atomicAdd(&distance[i], bitCount(x[index] ^ y[index]));
	}
}

__global__ void total(uint64_t* input, int* output, int len) {
	//@@ Load a segment of the input vector into shared memory
	__shared__ unsigned int partialSum[2 * BLOCK_SIZE];
	unsigned int t = threadIdx.x, start = 2 * blockIdx.x * BLOCK_SIZE;
	if (start + t < len)
		partialSum[t] = bitCount(input[start + t]);
	else
		partialSum[t] = 0;
	if (start + BLOCK_SIZE + t < len)
		partialSum[BLOCK_SIZE + t] = bitCount(input[start + BLOCK_SIZE + t]);
	else
		partialSum[BLOCK_SIZE + t] = 0;
	//@@ Traverse the reduction tree
	for (unsigned int stride = BLOCK_SIZE; stride >= 1; stride >>= 1) {
		__syncthreads();
		if (t < stride)
			partialSum[t] += partialSum[t + stride];
	}
	//@@ Write the computed sum of the block to the output vector at the 
	//@@ correct index
	if (t == 0)
		output[blockIdx.x] = partialSum[0];
}


//Used when GPU memory usage is bigger than 1 gigabyte
int cudaLargeMemUsage(int length, int realN, int numOutputElements, int blocksCount, int* distance, int* d_distance, int** output, int** d_output, uint64_t** x, uint64_t** d_x, int* sum)
{
	cudaError_t cudaStatus;

	printf("Large memory usage \n");
	cudaStatus = cudaMalloc(&d_distance, length * length * sizeof(int));
	if (cudaStatus != cudaSuccess)
	{
		fprintf(stderr, "cudaMalloc failed!");
		goto Error;
	}

	cudaStatus = cudaMemset(d_distance, 0, length * length * sizeof(int));
	if (cudaStatus != cudaSuccess)
	{
		fprintf(stderr, "cudaMemset failed!");
		goto Error;
	}

	for (int i = 0; i < 2; i++)
	{
		cudaStatus = cudaMalloc(&d_x[i], realN * sizeof(uint64_t));
		if (cudaStatus != cudaSuccess)
		{
			fprintf(stderr, "cudaMalloc failed!");
			goto Error;
		}
	}

	for (int i = 0; i < length; i++)
	{
		cudaStatus = cudaMalloc(&d_output[i], sizeof(int) *  numOutputElements);
		if (cudaStatus != cudaSuccess)
		{
			fprintf(stderr, "cudaMalloc failed!");
			goto Error;
		}

		cudaMemset(d_output[i], 0, sizeof(int) * numOutputElements);
		if (cudaStatus != cudaSuccess)
		{
			fprintf(stderr, "cudaMemset failed!");
			goto Error;
		}
	}

	printf("Copying completed\n");

	sum = new int[length];
	memset(sum, 0, length * sizeof(int));

	for (int i = 0; i < length; i++)
	{
		cudaStatus = cudaMemcpy(d_x[0], x[i], realN * sizeof(uint64_t), cudaMemcpyHostToDevice);
		if (cudaStatus != cudaSuccess)
		{
			fprintf(stderr, "cudaMemcpy failed!");
			goto Error;
		}

		total << <numOutputElements, BLOCK_SIZE >> > (d_x[0], d_output[i], realN);

		cudaDeviceSynchronize();
		cudaStatus = cudaGetLastError();
		if (cudaStatus != cudaSuccess) {
			fprintf(stderr, "reduce launch failed: %s\n", cudaGetErrorString(cudaStatus));
			goto Error;
		}
	}


	for (int i = 0; i < length; i++)
	{
		cudaStatus = cudaMemcpy(output[i], d_output[i], numOutputElements * sizeof(int), cudaMemcpyDeviceToHost);
		if (cudaStatus != cudaSuccess)
		{
			fprintf(stderr, "cudaMemcpy failed!");
			goto Error;
		}
	}

	for (int i = 0; i < length; i++)
	{
		for (int j = 0; j < numOutputElements; j++)
			sum[i] += output[i][j];
	}

	for (int i = 0; i < length; i++)
	{
		cudaStatus = cudaMemcpy(d_x[0], x[i], realN * sizeof(uint64_t), cudaMemcpyHostToDevice);
		if (cudaStatus != cudaSuccess)
		{
			fprintf(stderr, "cudaMemcpy failed!");
			goto Error;
		}
		for (int j = i + 1; j < length; j++)
		{
			if (abs(sum[i] - sum[j]) == 1)
			{
				cudaStatus = cudaMemcpy(d_x[1], x[j], realN * sizeof(uint64_t), cudaMemcpyHostToDevice);
				if (cudaStatus != cudaSuccess)
				{
					fprintf(stderr, "cudaMemcpy failed!");
					goto Error;
				}
				comparePair << <blocksCount, BLOCK_SIZE >> > (realN, d_x[0], d_x[1], d_distance, i*length + j);

				cudaDeviceSynchronize();
				cudaStatus = cudaGetLastError();
				if (cudaStatus != cudaSuccess) {
					fprintf(stderr, "comparePair launch failed: %s\n", cudaGetErrorString(cudaStatus));
					goto Error;
				}
			}
		}
	}


	cudaStatus = cudaMemcpy(distance, d_distance, length * length * sizeof(int), cudaMemcpyDeviceToHost);
	if (cudaStatus != cudaSuccess)
	{
		fprintf(stderr, "cudaMemcpy failed!");
		goto Error;
	}

	return 0;

Error:
	cudaFree(d_x);
	cudaFree(d_distance);
	cudaDeviceReset();
	free(distance);
	free(x);

	return -1;
}

int cudaNormalMemUsage(int length, long  realN, int numOutputElements, int blocksCount, int* distance, int* d_distance, int** output, int** d_output, uint64_t** x, uint64_t** d_x, int* sum)
{
	cudaError_t cudaStatus;

	cudaStatus = cudaMalloc(&d_distance, length * length * sizeof(int));
	if (cudaStatus != cudaSuccess)
	{
		fprintf(stderr, "cudaMalloc failed!");
		goto Error;
	}

	cudaStatus = cudaMemset(d_distance, 0, length * length * sizeof(int));
	if (cudaStatus != cudaSuccess)
	{
		fprintf(stderr, "cudaMemset failed!");
		goto Error;
	}

	for (int i = 0; i < length; i++)
	{
		cudaStatus = cudaMalloc(&d_x[i], realN * sizeof(uint64_t));
		if (cudaStatus != cudaSuccess)
		{
			fprintf(stderr, "cudaMalloc failed!");
			goto Error;
		}


		cudaStatus = cudaMalloc(&d_output[i], sizeof(int) *  numOutputElements);
		if (cudaStatus != cudaSuccess)
		{
			fprintf(stderr, "cudaMalloc failed!");
			goto Error;
		}

		cudaMemset(d_output[i], 0, sizeof(int) * numOutputElements);
		if (cudaStatus != cudaSuccess)
		{
			fprintf(stderr, "cudaMemset failed!");
			goto Error;
		}

		cudaStatus = cudaMemcpy(d_x[i], x[i], realN * sizeof(uint64_t), cudaMemcpyHostToDevice);
		if (cudaStatus != cudaSuccess)
		{
			fprintf(stderr, "cudaMemcpy failed!");
			goto Error;
		}

	}

	printf("Copying completed\n");

	sum = new int[length];
	memset(sum, 0, length * sizeof(int));

	for (int i = 0; i < length; i++)
	{
		total << <numOutputElements, BLOCK_SIZE >> > (d_x[i], d_output[i], realN);
	}

	cudaStatus = cudaGetLastError();
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "reduce launch failed: %s\n", cudaGetErrorString(cudaStatus));
		goto Error;
	}

	for (int i = 0; i < length; i++)
	{
		cudaStatus = cudaMemcpy(output[i], d_output[i], numOutputElements * sizeof(int), cudaMemcpyDeviceToHost);
		if (cudaStatus != cudaSuccess)
		{
			fprintf(stderr, "cudaMemcpy failed!");
			goto Error;
		}
	}

	for (int i = 0; i < length; i++)
	{
		for (int j = 0; j < numOutputElements; j++)
			sum[i] += output[i][j];
	}


	for (int i = 0; i < length; i++)
	{
		for (int j = i + 1; j < length; j++)
		{
			if (abs(sum[i] - sum[j]) == 1)
			{
				comparePair << <blocksCount, BLOCK_SIZE >> > (realN, d_x[i], d_x[j], d_distance, i*length + j);

				cudaStatus = cudaGetLastError();
				if (cudaStatus != cudaSuccess) {
					fprintf(stderr, "comparePair launch failed: %s\n", cudaGetErrorString(cudaStatus));
					goto Error;
				}
			}
		}
	}


	cudaStatus = cudaMemcpy(distance, d_distance, length * length * sizeof(int), cudaMemcpyDeviceToHost);
	if (cudaStatus != cudaSuccess)
	{
		fprintf(stderr, "cudaMemcpy failed!");
		goto Error;
	}

	return 0;

Error:
	cudaFree(d_x);
	cudaFree(d_distance);
	cudaDeviceReset();
	free(distance);
	free(x);

	return -1;
}




int main()
{
	uint64_t N = (uint64_t)((uint64_t)1 << (uint64_t)22);		// Length of single sequnce in bits
	int length = 256;										    // Number of sequences
	int firstBlock = N % 64;
	int realN = N / 64;
	if (firstBlock != 0)
		realN++;
	int blocksCount = (realN + BLOCK_SIZE - 1) / BLOCK_SIZE;

	int numOutputElements = realN / (BLOCK_SIZE << 1);
	if (realN % (BLOCK_SIZE << 1)) {
		numOutputElements++;
	}

	bool largeMemoryUsage = ((uint64_t)realN * (uint64_t)8 * (uint64_t)length) > (1 << 30);

	uint64_t **x = NULL, **d_x = NULL;
	int *distance = NULL, *d_distance = NULL, **d_output = NULL, **output = NULL, *sum = NULL;
	cudaError_t cudaStatus;
	printf("Generating %d random sequences with length %" PRIu64 "\n", length, N);

	x = (uint64_t**)malloc(length * sizeof(uint64_t*));
	d_x = (uint64_t**)malloc(length * sizeof(uint64_t*));
	distance = new int[length*length];

	d_output = (int**)malloc(length * sizeof(int*));
	output = (int**)malloc(length * sizeof(int*));
	srand(1234);
	for (int i = 0; i < length; i++)
	{
		srand(1234 + i);
		x[i] = (uint64_t*)malloc(realN * sizeof(uint64_t));
		output[i] = (int*)malloc(sizeof(int) * blocksCount);
		for (int j = 0; j < realN; j++)
		{
			if (j == 0 && firstBlock != 0)
			{
				for (int k = firstBlock; k >= 0; k--)
				{
					x[i][j] += (uint64_t)(rand() % 2) << k;
				}
			}
			else
			{
				for (int k = 63; k >= 0; k--)
				{
					x[i][j] += (uint64_t)(rand() % 2) << k;
				}
			}
		}
	}
	printf("Random sequences generated\nGPU: \nCopying data to GPU\n");

	// ----------------------------------------------------------------------------- GPU ------------------------------------------------------------------------------------
	clock_t start = clock();

	if (largeMemoryUsage)
		cudaLargeMemUsage(length, realN, numOutputElements, blocksCount, distance, d_distance, output, d_output, x, d_x, sum);
	else
		cudaNormalMemUsage(length, realN, numOutputElements, blocksCount, distance, d_distance, output, d_output, x, d_x, sum);

	clock_t end = clock();
	float seconds = (float)(end - start) / CLOCKS_PER_SEC;
	printf("Time: %f\n", seconds);
	int pary = 0;

	for (int i = 0; i < length; i++)
		for (int j = i + 1; j < length; j++)
		{
			if (distance[i * length + j] == 1)
				pary++;
		}
	printf("Found %d pairs\n", pary);

	cudaFree(d_x);
	cudaFree(d_distance);
	cudaDeviceReset();
	free(distance);

	// ------------------------------------------------------------------------ CPU -----------------------------------------------------------------------------------------

	printf("\nCPU:\n");
	pary = 0;
	start = clock();
	distance = new int[length*length];
	memset(distance, 0, length * length * sizeof(int));

	for (int i = 0; i < length; i++)
	{
		for (int j = i + 1; j < length; j++)
		{
			for (int k = 0; k < realN; k++)
			{
				distance[i*length + j] += bitCount(x[i][k] ^ x[j][k]);
			}
		}
	}

	end = clock();
	seconds = (float)(end - start) / CLOCKS_PER_SEC;
	printf("Time: %f\n", seconds);

	for (int i = 0; i < length; i++)
		for (int j = i + 1; j < length; j++)
		{
			if (distance[i * length + j] == 1)
				pary++;
		}
	printf("Found %d pairs\n", pary);

	free(x);
	free(distance);
	return 0;
}
